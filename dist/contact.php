<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>

    <?php 
      include 'db_connection.php';

      if(isset($_POST['submit'])) {
        $nume = $_POST['nume'];
        $email = $_POST['email'];
        $text = $_POST['text'];
        $mesaj = $nume. ' a fost adugat cu succes!';
        $result = queryResult(
        "INSERT INTO contact (nume, email, text)
        VALUES ('$nume', '$email', '$text')
        ");
        if($result){
          echo "<script>alert('$mesaj');</script>"; 
        } else {
          echo 'A aparut o eroare';
        }
    }
    ?>
    
    <script src="main.js"></script>
    <div class="container">  
      <form id="contact" action="contact.php" method="post">
        <h3>Formular Acoperisuri Bune</h3>
        <h4>Contactati-ne prin acest formular</h4>
        <fieldset>
          <input placeholder="Numele Tau" name="nume" type="text" tabindex="1" required autofocus>
        </fieldset>
        <fieldset>
          <input placeholder="Email" name="email" type="email" tabindex="2" required>
        </fieldset>
        <fieldset>
          <textarea placeholder="Introduceti textul...." name="text" tabindex="5" required></textarea>
        </fieldset>
        <fieldset>
          <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Trimite-ti</button>
        </fieldset>
      </form>
    </div> 
    </body>
</html>